import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'mobx-react';
import { LocaleProvider } from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import BasicLayout from './layouts/basicLayout';

class App extends Component {
  render() {
    return (
      <Router>
        <LocaleProvider locale={zh_CN}>
          <Provider>
            <Switch>
              <Route exact={true} path="/" component={BasicLayout}></Route>
            </Switch>
          </Provider>
        </LocaleProvider>
      </Router>
    );
  }
}

export default App;