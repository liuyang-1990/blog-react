import React from 'react';
import { Layout } from 'antd';
import './footer.less';

const { Footer } = Layout;

interface IGlobalFooterProps {
    links?: Array<{
        key?: string;
        title: React.ReactNode;
        href: string;
        blankTarget?: boolean;
    }>;
    copyright?: React.ReactNode;
}

export default class FooterView extends React.Component<IGlobalFooterProps, any>{

    render() {

        const { links, copyright } = this.props;
        return (
            <Footer style={{ padding: 0 }}>
                <footer className="globalFooter">
                    {links && (
                        <div className="links">
                            {links.map(link => (
                                <a
                                    key={link.key}
                                    title={link.key}
                                    target={link.blankTarget ? '_blank' : '_self'}
                                    href={link.href}>
                                    {link.title}
                                </a>
                            ))}
                        </div>
                    )}
                    {copyright && <div className={"copyright"}>{copyright}</div>}
                </footer>
            </Footer>
        )
    }
}

