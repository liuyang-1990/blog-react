import React, { Component, Fragment } from 'react';
import { BackTop, Icon } from 'antd';
import ProLayout, { MenuDataItem } from '@ant-design/pro-layout';
import logo from "../assests/logo.svg";
import { Link } from 'react-router-dom';
import Footer from './footer';
import { menus } from "../config/menus.config";
import ContentMain from './contentMain';

export default class BasicLayout extends Component<any, any> {

    render() {

        const menuDataRender = (menuList: MenuDataItem[]): MenuDataItem[] => {
            return menuList.map(item => {
                const localItem = { ...item, children: item.children ? menuDataRender(item.children) : [] };
                return localItem as MenuDataItem;
            });
        };
        const links = [{
            key: 'Ant Design Pro',
            title: 'Ant Design Pro',
            href: 'https://pro.ant.design',
            blankTarget: true,
        },
        {
            key: 'github',
            title: <Icon type="github" />,
            href: 'https://gitlab.com/liuyang-1990/blog-react',
            blankTarget: true,
        },
        {
            key: 'Ant Design',
            title: 'Ant Design',
            href: 'https://ant.design',
            blankTarget: true,
        }];

        const copyright = (
            <Fragment>
                Copyright <Icon type="copyright" /> 2019 created by liuyang
            </Fragment>
        );
        return (
            <ProLayout
                logo={logo}
                title="liuyang's  Blog"
                theme="light"
                layout="topmenu"
                contentWidth="Fixed"
                fixedHeader={true}
                route={menus}
                menuItemRender={(menuItemProps, defaultDom) => {
                    return <Link key={menuItemProps.path} to={menuItemProps.path}>{defaultDom}</Link>;
                }}
                menuDataRender={menuDataRender}
                footerRender={footerProps => <Footer links={links} copyright={copyright} {...footerProps} />}
                {...this.props}
            >
                <BackTop visibilityHeight={100} />
                <ContentMain />
            </ProLayout>
        )
    }

}