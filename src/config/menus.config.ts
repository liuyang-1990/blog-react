const menus: any =
{
    routes: [
        {
            name: '首页',
            path: '/'
        },
        {
            name: '文章详情',
            hideInMenu: true,
            path: '/articleDetail'
        },
        {
            name: '历程',
            path: '/timeline'
        },
        {
            name: '留言',
            path: '/message'
        },
        {
            name: '关于我',
            path: '/about'
        }
    ]
}


export { menus };